var api = require('./config/api');
var db = require('./config/database');

db.connect(function() {
  api.app.listen(8000);
});
