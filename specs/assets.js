var should = require('should');
var request = require('supertest');
var api = require('../config/api').app;
var db = require('../config/database');

describe('/assets', function() {
  before(function(done) {
    db.connect(done);
  });

  after(function() {
    db.disconnect();
  });

  describe('POST', function() {
    it('should correctly create asset', function(done) {
      request(api)
        .post('/assets')
        .attach('image', 'specs/files/test.png')
        .expect(201)
        .end(function(err, res) {
          if (err)
            return done(err);
          should(res.body.id).be.a.String;
          done();
        });
    });

    it('should error with missing file', function(done) {
      request(api)
        .post('/assets')
        .expect(400)
        .end(function(err, res) {
          if (err)
            return done(err);
          done();
        });
    });
  });

  describe('GET', function() {
    it('should correctly get asset', function(done) {
      request(api)
        .post('/assets')
        .attach('image', 'specs/files/test.png')
        .expect(201)
        .end(function(err, res) {
          if (err)
            return done(err);
          request(api)
            .get('/assets/' + res.body.id)
            .expect(200)
            .end(function(err, res) {
              if (err)
                return done(err);

              done();
            });
        });
    });

    it('should error with missing id', function(done) {
      request(api)
        .get('/assets')
        .expect(404)
        .end(function(err, res) {
          if (err)
            return done(err);

          done();
        });
    });
  });

  describe('DELETE', function() {
    it('should correctly delete asset', function(done) {
      request(api)
        .delete('/assets/opazkdzoakdpozakd')
        .expect(204)
        .end(function(err, res) {
          if (err)
            return done(err);

          done();
        });
    });

    it('should error with missing id', function(done) {
      request(api)
        .delete('/assets')
        .expect(404)
        .end(function(err, res) {
          if (err)
            return done(err);

          done();
        });
    });
  });
});
