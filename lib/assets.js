var express = require('express');
var app = require('../config/api').app;
var db = require('../config/database');
var streamifier = require('streamifier');

var router = express.Router();

router.post('/', function(req, res) {
  if (req.files[Object.keys(req.files)[0]] === undefined) {
    res.status(400);
    res.end();
  } else {
    var stream = app.get('gfs').createWriteStream({
      filename: req.files[Object.keys(req.files)[0]].name
    });
    stream.on('close', function(file) {
      res.status(201);
      res.send({
        id: file._id
      });
    });
    streamifier.createReadStream(req.files[Object.keys(req.files)[0]].buffer).pipe(stream);
  }
});

router.delete('/:id', function(req, res) {
  app.get('gfs').remove({
    _id: req.params.id
  }, function(err) {
    if (err)
      res.status(404);
    else
      res.status(204);
    res.end();
  });
});

router.get('/:id', function(req, res) {
  var stream = app.get('gfs').createReadStream({
    _id: req.params.id
  });
  stream.on('error', function() {
    res.status(404);
    res.end();
  });
  stream.on('end', function() {
    res.status(200);
    res.end();
  });
  stream.pipe(res);
});

module.exports = router;
