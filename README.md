# TV-Resources
## Description
This is the microservice used to manage assets in the backoffice of the TV Project.
## API
### Creation
#### POST on /assets
POST on /assets a multipart request containing the file you want to store in the backoffice.

this will response a json like this:
```json
{
  "id": "idofthefile"
}
```
### Get
#### GET on /assets/:id
GET on /assets/:id where id is the id of the file

This will response with the file content
### Deletion
#### DELETE on /assets/:id
DELETE on /assets/:id where id is the id of the file
